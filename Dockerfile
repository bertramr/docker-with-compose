FROM docker:20

RUN apk add --no-cache \
                        bash=5.1.16-r0 \
                        py3-pip=20.3.4-r1

RUN wget -q https://github.com/docker/compose/releases/download/v2.2.3/docker-compose-linux-x86_64 -O /bin/docker-compose \
 && chmod +x /bin/docker-compose \
 && mkdir -p /usr/local/lib/docker/cli-plugins/ \
 && ln -s /bin/docker-compose /usr/local/lib/docker/cli-plugins/ \
 && docker compose version
